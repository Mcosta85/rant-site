(ns rantsite.web
  (:require [compojure.core :refer [defroutes GET]]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.adapter.jetty :as ring]
            [ring.middleware.json :as json]
            [rantsite.controllers.resource :as res]
            [rantsite.controllers.vote :as vote]
            [rantsite.controllers.ranter :as ranter]
            [rantsite.controllers.rant :as rant]
            [rantsite.controllers.comment :as comment]
            [rantsite.controllers.tag :as tag]))

(defroutes app-routes
  rant/routes
  ranter/routes
  comment/routes
  vote/routes
  tag/routes
  res/routes)

(def app
  (->
    (handler/api app-routes)
    (json/wrap-json-body)
    (json/wrap-json-response)))


(defn start []
  (ring/run-jetty #'app {:port 8080
                         :join? false}))

(defn -main []
  (start))
