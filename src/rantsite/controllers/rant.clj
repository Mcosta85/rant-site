(ns rantsite.controllers.rant
  (:use ring.util.response)
  (:require [compojure.core :refer [context defroutes GET POST PUT DELETE]]
            [compojure.route :as route]
            [clojure.walk :as walk]
            [rantsite.models.rant :as model]
            [rantsite.models.rant-tags :as tag]
            [rantsite.utilities.common :as utils]))

(defn ^:private all
  "Returns all rants. By default, they are paginated 20 at a time. Optional page-size and offset params allow for customized fetching."
  [get-request]
  (let [request (walk/keywordize-keys (:body get-request))
        page-size (:page-size request utils/default-page-size)
        offset (:offset request utils/default-offset)]
  (model/get-all page-size 
                 offset
                 utils/db-spec)))

(defn ^:private all-by-ranter
  "Returns all rants by a ranter."
  [ranter-id]
  (try
    (model/get-all-by-ranter (read-string ranter-id)
                                          utils/db-spec)

    (catch AssertionError e {:success false
                             :message "Invalid ranter id."})))

(defn ^:private one
  "Returns the rant specified by the given id."
  [rant-id]
  (try
    (model/get-one (read-string rant-id
                                utils/db-spec))

    (catch AssertionError e {:success false
                             :message "Rant id must be a number"})))

(defn ^:private create!
  "Adds the rant to the db."
  [rant-request]
  (try
    (let [request-body (walk/keywordize-keys (:body rant-request))
          tags (tag/extract-tags-from-rant (:body request-body))
          rant (model/create! request-body
                              utils/db-spec)]

      (when
        (= true (:success rant))
        (tag/add-tags-to-rant (:id rant) tags)))

    (catch AssertionError e {:success false
                             :message "All fields must not be blank."})))

(defn ^:private update
  "edits the given rant with the supplied information."
  [rant-id request]
  (try
    (model/update! (read-string rant-id) (walk/keywordize-keys
                                           (:body request)))

    (catch AssertionError e {:success false
                             :message "No field updates provided."})))

(defn ^:private soft-delete
  "Changes the specified rants status to deleted. This rant will no longer be displayed."
  [id]
  (model/soft-delete! (read-string id)))

(defn ^:private hard-delete
  "Permanently removes the given rant from the db."
  [id]
  (model/hard-delete! (read-string id)))

(defn ^:private reactivate
  "Changes the specified rant's status to active."
  [rant-id]
  (model/update! (read-string rant-id) {:status_id utils/active}))

(defroutes routes
  (context "/api" []
    (context "/rants" []
      (GET "/" request (response (all request)))
      (POST "/" request (response (create! request))) 

      (context "/:id" [id]
        (GET "/" [] (response (one id)))
        (PUT "/" request (response (update id request)))
        (DELETE "/" [] (response (soft-delete id)))
        (DELETE "/delete" [] (response (hard-delete id)))
        (PUT "/reactivate" [] (response (reactivate id)))))

    (GET "/ranter/:id/rants" [id] (response (all-by-ranter id)))))
