(ns rantsite.controllers.ranter
  (:use ring.util.response)
  (:require [compojure.core :refer [context defroutes GET POST PUT DELETE]]
            [compojure.route :as route]
            [clojure.walk :as walk]
            [rantsite.utilities.common :as utils]
            [rantsite.models.ranter :as model]))

(defn ^:private one
  "Returns the ranter with the specified id."
  [ranter-id]
  (try
    (model/get-one (read-string ranter-id) utils/db-spec)

    (catch AssertionError e {:success false
                             :message "Ranter id must be a number."})))

(defn ^:private create
  "Adds a ranter to the db."
  [ranter-request]
  (try
    (let [response (model/create! (walk/keywordize-keys 
                                    (:body ranter-request))
                                  utils/db-spec)]
      (-> response
          (status 201)
          (header "Location" (str "/ranters/" (:id response)))))

    (catch AssertionError e {:success false
                             :message "Fields must not be blank."})))

(defn ^:private update
  "Edits the given ranter with info contained in the request."
  [id ranter-request]
  (try
  (model/update! (read-string id) (walk/keywordize-keys
                                  (:body ranter-request)) 
                                  utils/db-spec)

    (catch AssertionError e {:success false
                             :message "No fields updated."})))

(defn ^:private hard-delete
  "Permanently delete the given ranter from the db. If you want the user to be able to reinstate their account, use soft-delete-ranter."
  [id]
  (model/hard-delete! (read-string id)
                      utils/db-spec))

(defn ^:private soft-delete
  "Changes the specified user's status to deleted. The user will still exist, but their rants and other info will be unreachable."
  [id]
  (model/soft-delete! (read-string id)
                      utils/db-spec))

(defn ^:private reactivate
  "Changes the specified ranter's status to active."
  [ranter-id]
  (model/update! (read-string ranter-id)
                 {:status_id utils/active}
                 utils/db-spec))

(defroutes routes
  (route/resources "/")

  (context "/api" []
    (context "/ranters" []
      (POST "/" request (response (create request)))

      (context "/:id" [id]
        (GET "/" [] (response (one id)))
        (PUT "/" request (response (update id request)))
        (DELETE "/" [] (response
                         (soft-delete id)))

        (DELETE "/delete" [] (response
                                 (hard-delete id)))
        (PUT "/reactivate" [] (response (reactivate id)))))))


