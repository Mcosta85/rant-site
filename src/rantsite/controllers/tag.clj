(ns rantsite.controllers.tag
  (:use ring.util.response)
  (:require [compojure.core :refer [context defroutes GET POST]]
            [clojure.walk :as walk]
            [rantsite.models.tag :as model]
            [rantsite.utilities.common :as utils]))

(defn ^:private all
  "Returns all tags. By default, they are paginated 20 at a time. Optional page-size and offset params allow for customized fetching."
  [get-request]
  (let [request (walk/keywordize-keys (:body get-request))
        page-size (:page-size request utils/default-page-size)
        offset (* page-size (:offset request utils/default-offset))]
    (model/get-all page-size offset)))

(defn ^:private create
  "Adds the given tag to the db."
  [tag-request]
  (try
    (model/create! (walk/keywordize-keys (:tag-name (:body tag-request))))

    (catch AssertionError e {:success false
                             :message "All fields must not be blank."})))

(defroutes routes
  (context "/api/tags" []
    (GET "/" request (response (all request)))
    (POST "/" request (response (create request)))))
