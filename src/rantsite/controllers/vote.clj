(ns rantsite.controllers.vote
  (:use ring.util.response)
  (:require [compojure.core :refer [context defroutes GET POST PUT DELETE]]
            [compojure.route :as route]
            [clojure.walk :as walk]
            [rantsite.models.vote :as model]
            [rantsite.utilities.common :as utils]))

(defn ^:private all
  "Gets the count of all up and down votes."
  [object rant-id]
  (try
    (model/get-count object (read-string rant-id))

    (catch AssertionError e {:success false
                             :message "Invalid input value."})))

(defn ^:private create
  "Adds the specified vote to the objects table."
  [object id vote-request]
  (let [request (walk/keywordize-keys (:body vote-request))
        ranter-id (:ranter-id request)
        vote-type-id (:vote-type-id request)]
    (try
      (model/create! object ranter-id (read-string id) vote-type-id)

      (catch AssertionError e {:success false
                               :message "Invalid input received."}))))

(defn ^:private update
  "Modifies the specified vote in the db."
  [object id vote-request]
  (let [request (walk/keywordize-keys (:body vote-request))
        ranter-id (:ranter-id request)
        vote-type-id (:vote-type-id request)]
    (try
      (model/update! object ranter-id (read-string id) vote-type-id)

      (catch AssertionError e {:success false
                               :message "Invalid update input."}))))

(defroutes routes
  (context "/api/rants/votes" []
    (context "/:id" [id]
      (GET "/" [] (response (all model/rant-object id)))
      (POST "/" request (response (create model/rant-object
                                          id
                                          request)))
      (PUT "/" request (response (update model/rant-object
                                         id
                                         request)))))

  (context "/api/comments/votes" []
    (context "/:id" [id]
      (GET "/" [] (response (all model/comment-object id)))
      (POST "/" request (response (create model/comment-object
                                          id
                                          request)))
      (PUT "/" request (response (update model/comment-object
                                         id
                                         request))))))
