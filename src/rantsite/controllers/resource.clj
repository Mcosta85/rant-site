(ns rantsite.controllers.resource
  (:use ring.util.response)
  (:require [compojure.core :refer [context defroutes GET]]
            [compojure.route :as route]))

(defroutes routes
  (context "/resources" []
    (context "/ranters" []
      (context "/create" []
        (GET "/:file-type/:file" [file-type file] (resource-response file
                                                   {:root (str "public/"
                                                               file-type)})))))

  (route/resources "/"))
