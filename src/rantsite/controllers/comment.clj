(ns rantsite.controllers.comment
  (:use ring.util.response)
  (:require [compojure.core :refer [context defroutes GET POST PUT DELETE]]
            [compojure.route :as route]
            [clojure.walk :as walk]
            [rantsite.utilities.common :as utils]
            [rantsite.models.comment :as model]))

(defn ^:private all
  "Retrieves all the comments for a given rant."
  [rant-id get-request]
  (let [request (walk/keywordize-keys (:body get-request))
        page-size (:page-size request utils/default-page-size)
        offset (:offset request utils/default-offset)]
    (try
      (model/get-all (read-string rant-id) page-size offset)

      (catch AssertionError e {:success false
                               :message "Only numeric values accepted."}))))

(defn ^:private create
  "Adds a comment to the specified rant."
  [rant-id comment-request]
  (try
    (model/create! (read-string rant-id) (walk/keywordize-keys (:body comment-request)))

    (catch AssertionError e {:success false
                             :message "All fields require values."})))

(defn ^:private update
  "Edits the specified comment with the new information given."
  [comment-id update-request]
  (try
    (model/update! (read-string comment-id) (walk/keywordize-keys (:body update-request)))

    (catch AssertionError e {:success false
                             :message "No field updates received."})))

(defn ^:private delete
  "Deletes the specified comment"
  [comment-id]
  (model/hard-delete! (read-string comment-id)))

(defroutes routes
  (context "/api" []
    (context "/comments/:id" [id]
      (GET "/" request (response (all id
                                     request)))
      (POST "/" request (response (create id request)))
      (PUT "/" request (response (update id request)))
      (DELETE "/" [] (response (delete id))))))
