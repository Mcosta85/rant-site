(ns rantsite.models.comment
  (:require [clojure.java.jdbc :as jdbc]
            [rantsite.utilities.exception :as handler]
            [rantsite.utilities.common :as utils]))

(defn get-all
  "Returns all comments for the supplied rant."
  ([rant-id]
   (get-all rant-id
            utils/default-page-size
            utils/default-offset
            utils/db-spec))

  ([rant-id page-size offset spec]
  {:pre [(every? number? [rant-id page-size offset])]}
    (try
      (jdbc/with-db-connection [db spec]
        (assoc {:comments
          (jdbc/query db
                      ["SELECT * FROM comment
                       WHERE rant_id = ?
                       ORDER BY created_at desc
                       LIMIT ?
                       OFFSET ?" rant-id page-size offset])}
                 :success true))

      (catch Exception e (handler/handle-exception e)))))

(defn create!
  "Inserts the comment into the DB."
  [rant-id {:keys [body tagline ranter-id]} spec]
  {:pre [(every? (comp not empty?) [tagline body])]}

  (try
  (into {:success true}
    (jdbc/with-db-connection [db spec]
      (jdbc/insert! db
                    :comment
                    {:body body
                     :tagline tagline
                     :status_id utils/active
                     :ranter_id ranter-id
                     :rant_id rant-id})))

  (catch Exception e (handler/handle-exception e))))

(defn update!
  "Edits the comment with information provided."
  [comment-id updated-comment spec]
  {:pre [((comp not empty?) updated-comment)]}
  (try
    (utils/parse-row-count
      (jdbc/with-db-connection [db spec]
        (jdbc/update! db
                      :comment
                      (utils/sanitize-keys updated-comment)
                      ["id = ?" comment-id])))

    (catch java.sql.BatchUpdateException e
      (try
        (throw (.getNextException e))

        (catch Exception e (handler/handle-exception e))))))

(defn soft-delete!
  "Updates the comments status to deleted."
  [comment-id spec]
  (update! comment-id
           {:status_id utils/deleted}
           spec))

(defn hard-delete!
  "Removes the specified comment from the DB."
  [comment-id spec]
  (try
    (utils/parse-row-count
      (jdbc/with-db-connection [db spec]
        (jdbc/delete! db
                      :comment
                      ["id = ?" comment-id])))

    (catch Exception e (handler/handle-exception e))))
