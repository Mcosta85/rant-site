(ns rantsite.models.vote
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as string]
            [clojure.walk :as walk]
            [rantsite.utilities.exception :as handler]
            [rantsite.utilities.common :as utils]))

(def upvote 1)
(def downvote 2)
(def rant-object "rant")
(def comment-object "comment")

(defn sanitize-votes
  "Takes a list of maps, as returned by get-count, and turns it into a map of vote-type count pairs."
  [vote-seq]
  (->> vote-seq
       (map vals)
       flatten
       (apply hash-map)
       walk/keywordize-keys))

(defn get-count
  "Returns all votes for a specified object as a list of maps. Use the hepler function sanitize-votes from the count as a map of vote-type count pairs."
  [object id spec]
  {:pre [(not (string/blank? object))
         (every? utils/not-nil? [object id])
         (pos? id)
         (number? id)]}

  (let [vote-table (str object "_vote")
        query (str "SELECT count(*), vt.name
                    FROM " vote-table " as rv
                    LEFT JOIN vote_type as vt on vt.id = rv.vote_type_id"
                    " WHERE " object "_id = ?"
                    " GROUP BY vt.name")]

    (try
      (assoc
        (sanitize-votes
          (jdbc/with-db-connection [db spec]
            (jdbc/query db
                        [query id]
                        :transaction false)))
        :success true)
    (catch Exception e (handler/handle-exception e)))))

(defn create!
  "Creates a new vote in db and attaches it to specified object."
  [object ranter-id object-id vote-type-id spec]
  {:pre [(not (empty? object))
         (every? utils/not-nil? [ranter-id object-id vote-type-id])
         (every? pos? [ranter-id object-id vote-type-id])]}

  (let [vote-table (str object "_vote")
        vote-id (str object "_id")]
    (try
      (into {:success true}
        (jdbc/with-db-connection [db spec]
          (jdbc/insert! db
                        (keyword vote-table)
                        {(keyword vote-id) object-id
                         :ranter_id ranter-id
                         :vote_type_id vote-type-id})))

      (catch Exception e (handler/handle-exception e)))))

(defn update!
  "Changes the vote type in the db with the specified type."
  [{:keys [object ranter-id object-id vote-type-id]} spec]
  {:pre [(not (empty? object))
         (every? utils/not-nil? [ranter-id object-id vote-type-id])
         (every? pos? [ranter-id object-id vote-type-id])]}

  (let [vote-table (str object "_vote")
        vote-id (str object "_id")]

    (try
      (utils/parse-row-count
        (jdbc/with-db-connection [db spec]
          (jdbc/update! db
                        (keyword vote-table)
                        {:vote_type_id vote-type-id}
                        [(str vote-id " = ? AND ranter_id = ?")
                         object-id ranter-id])))

      (catch Exception e (handler/handle-exception e)))))
