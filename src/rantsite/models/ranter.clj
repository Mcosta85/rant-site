(ns rantsite.models.ranter
  (:require [clojure.java.jdbc :as jdbc]
            [rantsite.utilities.exception :as handler]
            [rantsite.utilities.common :as utils]))

(defn get-one
  "Retrieves the selected ranter."
  [ranter-id spec]
  {:pre [(number? ranter-id)]}
  (try
    (assoc {:ranter 
            (into {}
      (jdbc/with-db-connection [db spec]
        (jdbc/query db
                    ["SELECT r.display_name, r.created_at, s.name as status
                     FROM ranter as r
                     JOIN status as s
                     ON r.status_id = s.id
                     WHERE r.id = ?" ranter-id])))}
      :success true)

    (catch Exception e (handler/handle-exception e))))

(defn create!
  "Inserts a user into the db. Uses pre conditions to validate
  input. Consuming code should catch these."
  [{:keys [display-name password]} spec]
  {:pre [(every? (comp not empty?) [display-name
                                    password])]}

  (try
    (dissoc
      (into {:success true}
        (jdbc/with-db-connection [db spec]
          (jdbc/insert! db
                        :ranter
                        {:display_name display-name
                         :password_hashed password
                         :status_id utils/active})))
      :password_hashed :status_id :created_at)

   (catch Exception e (handler/handle-exception e))))

(defn update!
  "Edits the user with the information provided."
  [ranter-id updated spec]
  {:pre [((comp not empty?) updated)]}
  (try
    (utils/parse-row-count
      (jdbc/with-db-connection [db spec]
        (jdbc/update! db
                      :ranter
                      (utils/sanitize-keys updated)
                      ["id = ?" ranter-id])))

    (catch Exception e (handler/handle-exception e))))

(defn soft-delete!
  "Changes the users status to deleted. All information is 
  in the db, but the user is unable to be accessed in any way."
  [ranter-id spec]
  (update! ranter-id {:status_id 3} spec))

(defn hard-delete!
  "Removes the specified user from the database."
  [ranter-id spec]
  (try
    (utils/parse-row-count
      (jdbc/with-db-connection [db spec]
        (jdbc/delete! db
                      :ranter
                      ["id=?" ranter-id])))

    (catch Exception e (handler/handle-exception e))))
