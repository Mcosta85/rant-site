(ns rantsite.models.rant
  (:require [clojure.java.jdbc :as jdbc]
            [rantsite.utilities.exception :as handler]
            [rantsite.utilities.common :as utils]))

(defn get-one
  "Gets the rant with the supplied ID."
  [rant-id spec]
  {:pre [(number? rant-id)]}
  (try
    (assoc {:rant
            (into []
              (jdbc/with-db-connection [db spec]
                (jdbc/query db
                            ["SELECT * FROM rant WHERE id = ?" rant-id])))}
           :success true)

    (catch Exception e (handler/handle-exception e))))

(defn get-all
  "returns all active rants. Takes an optional JSON doc with offset and page-size keys to tweak how and what results are returned."
  ([]
   (get-all utils/default-page-size
            utils/default-offset
            utils/db-spec))

  ([page-size offset spec]
   (let [skip (* page-size offset)]
     (try
       (assoc {:rants
               (into []
                 (jdbc/with-db-connection  [db spec]
                   (jdbc/query db
                           ["SELECT * FROM rant
                            WHERE status_id <> 3
                            ORDER BY created_at desc
                            LIMIT ?
                            OFFSET ?" page-size offset])))}
              :success true)
    (catch Exception e (handler/handle-exception e))))))

(defn get-all-by-ranter
  "Retrieves all rants by the given ranter."
  [ranter-id spec]
  {:pre [(number? ranter-id)]}
  (try
    (assoc
      {:rants
      (into []
        (jdbc/with-db-connection [db spec]
          (jdbc/query db
                      ["SELECT * FROM rant
                       WHERE ranter_id = ?
                       ORDER BY created_at desc" ranter-id])))}
    :success true) 

    (catch Exception e (handler/handle-exception e))))

(defn create!
  "Inserts a rant into the DB."
  [{:keys [ranter-id tagline body]} spec]
  {:pre [(every? (comp not empty?) [tagline body])]}

  (try
    (into {:success true}
      (jdbc/with-db-connection [db spec]
        (jdbc/insert! db
                      :rant
                      {:ranter_id ranter-id
                       :tagline tagline
                       :body body
                       :status_id utils/active})))

    (catch Exception e (handler/handle-exception e))))

(defn update!
  "Updates a rant with provided information."
  [rant-id updated-rant spec]
  {:pre [((comp not empty?) updated-rant)]}
  (try
    (utils/parse-row-count
      (jdbc/with-db-connection [db spec]
        (jdbc/update! db
                      :rant
                      (utils/sanitize-keys updated-rant)
                      ["id = ?" rant-id])))

    (catch Exception e (handler/handle-exception e))))

(defn hard-delete!
  "Removes the specified rant from the DB."
  [rant-id spec]
  (try
    (utils/parse-row-count
      (jdbc/with-db-connection [db spec]
        (jdbc/delete! db
                      :rant
                      ["id = ?" rant-id])))

   (catch Exception e (handler/handle-exception e))))

(defn soft-delete!
  "Changes the rants status to deleted, but doesn't remove the rant
  completely."
  [rant-id spec]
  (update! rant-id {:status_id 3} spec))

