(ns rantsite.models.rant-tags
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as string]
            [rantsite.utilities.exception :as handler]
            [rantsite.utilities.common :as utils]))

(defn ^:private prepare-coll-for-lookup
  "Takes a standard clojure seq of tags and formats them to work with the jdbc driver"
  [tags]
  (->> tags
       (map #(str "'" % "'"))
        (string/join ", " )))

(defn ^:private seqify-params
  "Takes a rant id and a seq of tags and returns a vector of vector pairs."
  [rant-id tags]
  (into []
    (map #(into [] [rant-id %]) tags)))

(defn ^:private lookup-tags
  "Takes a seq of tags and returns a seq of the tag ids."
  [tag-coll spec]
  (let [tags  (prepare-coll-for-lookup tag-coll)
        query (str "SELECT id FROM tag
                       WHERE name IN (" tags ")")]

    (->> (jdbc/with-db-connection [db spec]
           (jdbc/query db
                       [query]))
         (map vals)
         flatten)))

(defn extract-tags-from-rant
  "Searches a rant for any hashtags and returns them as a seq"
  [rant]
  (re-seq #"#[a-zA-Z0-9'/]+" rant))

(defn get-all-by-tag
  "Retrieves all rants that a tagged with the given tag"
  ([tag-id spec]
    (get-all-by-tag tag-id
                    {:page-size utils/default-page-size
                     :offset utils/default-offset}
                    spec))

  ([tag-id get-request spec]
    (try
      (let [request (:body get-request get-request)
            page-size (:page-size request utils/default-page-size)
            offset (* page-size (:offset request utils/default-offset))]

        (jdbc/with-db-connection [db spec]
          (jdbc/query db
                      ["SELECT * FROM rant_tags_view
                       WHERE tag_id = ?
                       ORDER BY created_at desc
                       LIMIT ?
                       OFFSET ?" tag-id page-size offset])))

      (catch Exception e (handler/handle-exception e)))))

(defn add-tags-to-rant
  "Associates a tag with a rant"
  [rant-id tags spec]
  {:pre [(number? rant-id)]}
  (try
    (let [tag-ids (lookup-tags tags)]
      (utils/parse-row-count
        (jdbc/with-db-connection [db spec]
          (apply jdbc/insert! db
                              :rant_tags
                              ["rant_id" "tag_id"]
                              (seqify-params rant-id (lookup-tags tags))))))

    (catch Exception e (handler/handle-exception e))))
