(ns rantsite.models.tag
  (:require [clojure.java.jdbc :as jdbc]
            [clojure.string :as string]
            [rantsite.utilities.exception :as handler]
            [rantsite.utilities.common :as utils]))

(def search-by-string "SELECT id
                       FROM tag
                       WHERE lower(name) = ?")

(def search-by-id "SELECT name
                   FROM tag
                   WHERE id = ?")

(defn get-one
  "Retrieves the selected tag. Search criteria can be either the tag id for developers or tag name for use in normal on site search."
  [search-criteria spec]
  (let [query (atom search-by-string)
        criteria (atom search-criteria)]
    (try
      (cond
        (number? search-criteria)
          (reset! query search-by-id)
        :else (reset! criteria (string/lower-case search-criteria)))

      (assoc {:tag
        (jdbc/with-db-connection [db spec]
            (jdbc/query db
                        [@query @criteria]))}
             :success true)

      (catch Exception e (handler/handle-exception e)))))

(defn get-all
  "Returns all tags. Takes an optional JSON doc with offset and page-size keys to tweak how and what results are returned. Offset is the number of pages to offset. I.E page size is 20, and offset of 1 would start on the 20th record."
  ([]
   (get-all utils/default-page-size
            utils/default-offset
            utils/db-spec))

  ([page-size offset spec]
  {:pre [(every? (comp not neg?) [page-size offset])]}
    (try
      (let [skip (* page-size offset)]
        (assoc {:tags 
                (into []
        (jdbc/with-db-connection [db spec]
          (jdbc/query db
                      ["SELECT * FROM tag
                       LIMIT ?
                       OFFSET ?" page-size skip])))}
               :success true))

      (catch Exception e (handler/handle-exception e)))))

(defn create!
  "Inserts the supplied tag into the db, if it doesn't already exist."
  [tags spec]
  {:pre [((comp not empty?) tags)]}

  (try
    (utils/parse-row-count
      (jdbc/with-db-connection [db spec]
        (apply jdbc/insert! db
                        :tag
                        ["name"]
                        tags)))

    (catch Exception e (handler/handle-exception e))))
