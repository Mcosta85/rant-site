(ns rantsite.utilities.common
  (:import com.mchange.v2.c3p0.ComboPooledDataSource)
  (:require [clojure.string :as string]))

(def active 1)
(def inactive 2)
(def deleted 3)

(def default-page-size 20)
(def default-offset 0)

(def info "log/info")
(def error "log/error")

(def db-spec {:classname "org.postgresql.Driver"
              :subprotocol "postgresql"
              :subname "//localhost:5432/rantsite"
              :user "djvpknfnxiutw"
              :password "zI4PhzZmROZELP0s9AfKkg-_6M"})

(def test-spec {:classname "org.postgresql.Driver"
                :subprotocol "postgresql"
                :subname "//localhost:5432/test"
                :user "dbtester"
                :password "8F6R79d9:!LXOLq,Ea)4gbX^8Dqnn-"})

(defn pool
  [spec]
  (let [cpds (doto (ComboPooledDataSource.)
               (.setDriverClass (:classname spec))
               (.setJdbcUrl (str "jdbc:" (:subprotocol spec) ":" (:subname spec)))
               (.setUser (:user spec))
               (.setPassword (:password spec))
               ;; expire excess connections after 30 minutes of inactivity:
               (.setMaxIdleTimeExcessConnections (* 30 60))
               ;; expire connections after 3 hours of inactivity:
               (.setMaxIdleTime (* 3 60 60)))]
    {:datasource cpds}))

(defn pooled-db [spec] (delay pool spec))

(defn db-connection [spec] (pooled-db spec))

(def not-nil? (comp not nil?))

(defn rows-affected?
  "Returns a boolean whose value depends on whether any rows were affected. "
  [result]
  {:pre [(not-nil? result)
         (number? result)]}
  (if
    (pos? result)
    true
    false))

(defn parse-row-count
  "Turns the seq of rows affected into a map reflected the same information"
  [update-result]
  {:pre [(every? number? update-result)]}

  (let [result (reduce + update-result)]

  {:success (rows-affected? result)
   :rows-affected result}))

(defn format-options
  [options]
  (apply hash-map options))

;; PSQL Exception Handling

(defn extract-message
  "Extracts the error message from a PSQL Exception."
  [exception]
  (->> exception
       (.getServerErrorMessage)
       (.getMessage)))

(defn constraint-violated?
  "Searches for a regex in an extracted PSQL Exception."
  [constraint exception]
  (->> (extract-message exception)
       (re-matcher constraint)
       re-find
       not-nil?))

(defn sanitize-keys
  "Removes any '-' characters and replaces them with '_'."
  [in-map]
  (let [clean-keys (map (comp #(string/replace % "-" "_") name) (keys in-map))]
    (apply hash-map
      (interleave
          (map keyword clean-keys)
          (vals in-map)))))

;; PSQL Exception Messages

(def foreign-key #"violates foreign key constraint")

(def duplicate-key #"duplicate key value violates unique constraint")

(def invalid-operator #"operator does not exist")

(def invalid-type #"but expression is of type")

(def invalid-relation #"does not exist")

(def foreign-key-delete #"is still referenced from table")

(def null-value #"null value in column")

(def permission-denied #"Permission denied")

;; PSQL Exception Partial Functions

(def foreign-key-constraint-violated?
  (partial constraint-violated? foreign-key))

(def duplicate-key-constraint-violated?
  (partial constraint-violated? duplicate-key))

(def invalid-operator-constraint-violated?
  (partial constraint-violated? invalid-operator))

(def invalid-type-constraint-violated?
  (partial constraint-violated? invalid-type))

(def invalid-relation-constraint-violated?
  (partial constraint-violated? invalid-relation))

(def foreign-key-delete-constraint-violated?
  (partial constraint-violated? foreign-key-delete))

(def null-value-constraint-violated?
  (partial constraint-violated? null-value))

(def permission-denied-constraint-violated?
  (partial constraint-violated? permission-denied))
