(ns rantsite.utilities.fixtures
  (:require [rantsite.models.ranter :as ranter]
            [rantsite.models.rant :as rant]
            [rantsite.models.tag :as tag]
            [rantsite.models.comment :as comment]
            [rantsite.utilities.common :as utils]
            [rantsite.utilities.exception :as handler]
            [clojure.java.jdbc :as jdbc]))

;; Create test objects functions

(defn create-test-ranter
  []
  (ranter/create! {:display-name "Test Ranter"
                  :password "Test Password"}
                  utils/test-spec))

(defn create-test-rant
  []
  (rant/create! {:ranter-id 1
                 :tagline "Rant Test"
                 :body "Rant Test"}
                utils/test-spec))

(defn create-test-comment
  []
  (comment/create! 1
                   {:ranter-id 1
                    :tagline "Test tagline"
                    :body "Test comment"}
                   utils/test-spec))

;; Delete test objects

(defn delete-ranters
  []
  (try
    (jdbc/with-db-connection [db utils/test-spec]
      (jdbc/delete! db
                    :ranter
                    ["id > 0"])
      (jdbc/execute! db
                     ["ALTER SEQUENCE ranter_id_seq RESTART WITH 1"]))
        (catch Exception e (handler/handle-exception e))))

(defn delete-rants
  []
  (try
    (jdbc/with-db-connection [db utils/test-spec]
      (jdbc/delete! db
                    :rant
                    ["id > 0"])
      (jdbc/execute! db
                     ["ALTER SEQUENCE rant_id_seq RESTART WITH 1"]))
    (catch Exception e (handler/handle-exception e))))

(defn delete-tags
  []
  (try
    (jdbc/with-db-connection [db utils/test-spec]
      (jdbc/delete! db
                    :tag
                    ["id > 0"])
      (jdbc/execute! db
                     ["ALTER SEQUENCE tag_id_seq RESTART WITH 1"]))
    (catch Exception e (handler/handle-exception e))))

(defn delete-comments
  []
  (try
    (jdbc/with-db-connection [db utils/test-spec]
      (jdbc/delete! db
                    :comment
                    ["id > 0"])
      (jdbc/execute! db
                     ["ALTER SEQUENCE comment_id_seq RESTART WITH 1"]))
    (catch Exception e (handler/handle-exception e))))

(defn delete-votes
  []
  (try
    (jdbc/with-db-connection [db utils/test-spec]
      (jdbc/delete! db
                    :rant_vote
                    ["rant_id <> 0"])
      (jdbc/delete! db
                    :comment_vote
                    ["comment_id <> 0"]))

    (catch Exception e (handler/handle-exception e))))

;; Each Fixtures

(defn ranter-each-fixture
  [f]
  (create-test-ranter)

  (f)

  (delete-ranters))

(defn rant-each-fixture
  [f]
  (create-test-ranter)
  (create-test-rant)

  (f)

  (delete-rants)
  (delete-ranters))

(defn tag-each-fixture
  [f]
  (tag/create! ["#testing"]
               utils/test-spec)
  (f)
  (delete-tags))

(defn comment-each-fixture
  [f]
  (create-test-ranter)
  (create-test-rant)
  (create-test-comment)
  
  (f)

  (delete-comments)
  (delete-rants)
  (delete-ranters))

(defn vote-each-fixture
  [f]
  (create-test-ranter)
  (create-test-rant)
  (create-test-comment)

  (f)

  (delete-comments)
  (delete-rants)
  (delete-ranters))
