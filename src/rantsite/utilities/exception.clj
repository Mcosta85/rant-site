(ns rantsite.utilities.exception
  (:require [rantsite.utilities.common :as utils]
            [rantsite.utilities.log :as log]
            [clojure.string :as string]
            [clojure.java.jdbc :as jdbc]))

(defn parse-exception
  [exception]
    (println (count (string/split (.getMessage exception) #"(\.)"))))

(defn handle-exception
  "Handles all exceptions from a central location. Any logging or formatted error returning takes place right here."
  [e]
  (future log/write utils/error "Exception")
  (cond
    (= java.lang.NullPointerException (type e))
      {:success false :message "Null values aren't allowed."}

    (= java.lang.IllegalArgumentException (type e))
      {:success false :message e}

    (= java.sql.BatchUpdateException (type e))
      (handle-exception (.getNextException e))

    (utils/invalid-relation-constraint-violated? e)
      {:success false :message "Column or relation does not exist."}

    (utils/invalid-type-constraint-violated? e)
      {:success false :message "Invalid input type."}

    (utils/duplicate-key-constraint-violated? e)
      {:success false :message "This object already exists."}

    (utils/invalid-operator-constraint-violated? e)
      {:success false :message "Invalid operator."}

    (utils/null-value-constraint-violated? e)
      {:success false :message "Null values aren't allowed."}

    (utils/foreign-key-constraint-violated? e)
      {:success false :message "Referenced object does not exist."}

    :else
      (future log/write utils/error e)))
