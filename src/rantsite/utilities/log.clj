(ns rantsite.utilities.log
  (:require [clojure.tools.logging :as log]))

(comment
(defn get-all
"Returns all logged events of a specific log level. Takes an optional map of critera for filter results."
  ([level]
    (get-all level {}))

  ([level filters]
    (let [db (auth)]
      (monger-coll/find-maps db (name level) filters)))))

(defn write
"Writes an event to the logging db."
  [log-fn event]
  (log-fn (str event)))
