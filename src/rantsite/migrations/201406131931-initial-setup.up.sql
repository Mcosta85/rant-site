CREATE TABLE status (
id SERIAL PRIMARY KEY,
name TEXT NOT NULL,
description TEXT NOT NULL
);

CREATE TABLE ranter (
id serial PRIMARY KEY,
display_name text NOT NULL,
password_hash text NOT NULL,
email text NOT NULL,
created_at timestamp DEFAULT now(),
status_id INT references status(id) NOT NULL
);

CREATE TABLE rant (
id SERIAL PRIMARY KEY,
predicate TEXT NOT NULL,
body TEXT NOT NULL,
created_at TIMESTAMP,
status_id INT references status(id) NOT NULL,
ranter_id INT references ranter(id) NOT NULL
);

CREATE TABLE vote_type (
id SERIAL PRIMARY KEY,
name TEXT NOT NULL,
desc TEXT NOT NULL,
status_id INT references status(id) NOT NULL
);

CREATE TABLE comment (
id SERIAL PRIMARY KEY,
body TEXT NOT NULL,
created_at TIMESTAMP NOT NULL,
predicate TEXT NOT NULL,
status_id INT references status(id) NOT NULL,
rant_id INT references rant(id) NOT NULL,
ranter_id INT references ranter(id) NOT NULL
);

CREATE TABLE rant_votes (
rant_id INT references rant(id) NOT NULL,
vote_type_id INT references vote_type(id) NOT NULL,
ranter_id INT references ranter(id) NOT NULL
);

CREATE TABLE comment_votes (
comment_id INT references rant(id) NOT NULL,
vote_type_id INT references vote_type(id) NOT NULL,
ranter_id INT references ranter(id) NOT NULL
);
