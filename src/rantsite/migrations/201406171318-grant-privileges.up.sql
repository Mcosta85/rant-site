grant select, update, insert, delete on rant to dbuser;
grant select, update, insert, delete on comment to dbuser;
grant select, update, insert, delete on rant_votes to dbuser;
grant select, update, insert, delete on comment_votes to dbuser;
grant select on status to dbuser;
grant select on vote_type to dbuser;
grant select, update on comment_id_seq to dbuser;
grant select, update on rant_id_seq to dbuser;
