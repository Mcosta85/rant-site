insert into status (name, description)
values ('Active', 'Active items are pretty self explanatory.');

insert into status (name, description)
values ('Inactive', 'Inactive items arent quite deleted, and not quite active.
They are very much a paradox');

insert into vote_type (name, description, status_id)
values ('Upvote', 'This rant rocks. The ranter paints the perfect portrait of anger, 
with just the right amount of venom and humor.', 1);

insert into vote_type (name, description, status_id)
values ('Downvote', 'You could not disagree more. This person is an absolute moron
who probably belongs on the business end of a rant him or herself.', 1);
