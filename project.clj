(defproject rantsite "0.1.0-SNAPSHOT"
  :description "This is the core of the rantsite web app."
  :url "http://www.rantsite.com"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.1.6"]
                 [org.clojure/java.jdbc "0.3.3"]
                 [org.clojure/data.json "0.2.5"]
                 [org.clojure/tools.logging "0.3.1"]
                 [log4j/log4j "1.2.17"]
                 [ring "1.2.2"]
                 [ring/ring-json "0.3.1"]
                 [ring/ring-core "1.2.2"]
                 [ring-mock "0.1.5"]
                 [com.mchange/c3p0 "0.9.2.1"]
                 [com.jolbox/bonecp "0.7.1.RELEASE"]
                 [org.slf4j/slf4j-log4j12 "1.5.0"]
                 [postgresql "9.3-1101.jdbc4"]]
  :plugins      [[lein-ring "0.8.11"]]
  :ring   {:handler rantsite.web/app}
:main ^:skip-aot rantsite.web)
