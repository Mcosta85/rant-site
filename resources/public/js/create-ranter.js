 /*** @jsx React.DOM */

    var CreateRanter = React.createClass({
      getInitialState:function(){
        return {data:[], name:'', password:''}
      },

      onChangeName:function(e){
        this.setState({name:e.target.value})
      },

      onChangePassword:function(e){
        this.setState({password:e.target.value})
      },

      handleSubmit:function(e){
        e.preventDefault();
        var ranterInfo = JSON.stringify({"display-name":this.state.name,
  "password":this.state.password});
        /*$.post('../src/tiradez/controllers/ranter.clj',ranterInfo.serialize());},*/
        $.ajax({
                type: 'POST',
                url: '/api/ranters',
                data: ranterInfo,
                contentType: 'application/json',
                dataType: 'json',
                success: function(data) {
                console.log('that worked' + data);
                this.setState({name:'',password:''});
                }.bind(this),
                error: function(xhr, status, err) {
                alert(this.props.url, status, err.toString());}.bind(this)
        });
    },

      render: function(){
        return (
                <div>
                  <h2>Whatever you need to say as anonymously as you need to say it</h2>
                  <form onSubmit={this.handleSubmit}>
                    <input id="name" type="text"  value={this.state.name} onChange={this.onChangeName} />
                    <input id="password" type="password" value={this.state.password} onChange={this.onChangePassword} />
                    <button>Rant</button>
                  </form>
                  <RanterList data={this.state.data} />
                </div>
               )
          }
    });

    var Ranter = React.createClass({
      render: function() {
        return (
          <div>
            <h2>{this.props.name}, {this.props.password}</h2>
          </div>
          )
          }
      });

    var RanterList = React.createClass({
      render:function(){
        var ranters = this.props.data.map(function(ranter){
          return <Ranter name={ranter.name} password={ranter.password} />
        });
        return (
                <div>
                  {ranters}
                </div>
                )
            }
       });

    React.renderComponent(<CreateRanter />, document.getElementById('displayRanter'))
