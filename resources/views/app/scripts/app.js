'use strict';

/**
 * @ngdoc overview
 * @name viewsApp
 * @description
 * # viewsApp
 *
 * Main module of the application.
 */

angular.module('tiradez', ['ngAnimate', 'ngCookies', 'ngResource', 'ngRoute', 'ngSanitize', 'ngTouch']).config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/create-ranter.html',
        controller: 'CreateRanter'
      })
      .when('/rantz', {
        templateUrl: 'views/rantz.html',
        controller: ''
      })
      .otherwise({
        redirectTo: '/'
      });
  });
