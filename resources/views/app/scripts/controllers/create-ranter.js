'use strict';

/**
 * @ngdoc function
 * @name viewsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the viewsApp
 */

(function() {
  angular.module('tiradez').controller('CreateRanter',['$scope', function ($scope) {
  $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  
  $scope.ranters = {};

  $scope.collect = function (ranter) {
    $scope.ranters = angular.copy (ranter);
    console.log ($scope.ranters);
    $scope.ranter.displayName = '';
    $scope.ranter.predicate = '';
    };

  }]);
}());
