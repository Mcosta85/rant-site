﻿ALTER TABLE status
  OWNER TO postgres;
GRANT ALL ON TABLE status TO postgres;
GRANT SELECT ON TABLE status TO djvpknfnxiutw;

ALTER TABLE ranter
  OWNER TO postgres;
GRANT ALL ON TABLE ranter TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ranter TO djvpknfnxiutw;

ALTER TABLE rant
  OWNER TO postgres;
GRANT ALL ON TABLE rant TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE rant TO djvpknfnxiutw;

ALTER TABLE comment
  OWNER TO postgres;
GRANT ALL ON TABLE comment TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE comment TO djvpknfnxiutw;

ALTER TABLE vote_type
  OWNER TO postgres;
GRANT ALL ON TABLE vote_type TO postgres;
GRANT SELECT ON TABLE vote_type TO djvpknfnxiutw;

ALTER TABLE rant_vote
  OWNER TO postgres;
GRANT ALL ON TABLE rant_vote TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE rant_vote TO djvpknfnxiutw;

ALTER TABLE comment_vote
  OWNER TO postgres;
GRANT ALL ON TABLE comment_vote TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE comment_vote TO djvpknfnxiutw;

ALTER TABLE tag
  OWNER TO postgres;
GRANT ALL ON TABLE tag TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE tag TO djvpknfnxiutw;

ALTER TABLE rant_tags
  OWNER TO postgres;
GRANT ALL ON TABLE rant_tags TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE rant_tags TO djvpknfnxiutw;

ALTER TABLE logs
  OWNER TO postgres;
GRANT ALL ON TABLE logs TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE logs TO djvpknfnxiutw;

ALTER TABLE rant_tags_view
  OWNER TO postgres;
GRANT ALL ON TABLE rant_tags_view TO postgres;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE rant_tags_view TO djvpknfnxiutw;


ALTER TABLE comment_id_seq
  OWNER TO postgres;
GRANT ALL ON TABLE comment_id_seq TO postgres;
GRANT SELECT, UPDATE ON TABLE comment_id_seq TO djvpknfnxiutw;

ALTER TABLE rant_id_seq
  OWNER TO postgres;
GRANT ALL ON TABLE rant_id_seq TO postgres;
GRANT SELECT, UPDATE ON TABLE rant_id_seq TO djvpknfnxiutw;

ALTER TABLE ranter_id_seq
  OWNER TO postgres;
GRANT ALL ON TABLE ranter_id_seq TO postgres;
GRANT SELECT, UPDATE ON TABLE ranter_id_seq TO djvpknfnxiutw;

ALTER TABLE tag_id_seq
  OWNER TO postgres;
GRANT ALL ON TABLE tag_id_seq TO postgres;
GRANT SELECT, UPDATE ON TABLE tag_id_seq TO djvpknfnxiutw;
