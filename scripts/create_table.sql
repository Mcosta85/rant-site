﻿CREATE TABLE status
(
  id serial NOT NULL,
  name text NOT NULL,
  description text NOT NULL,
  CONSTRAINT status_pkey PRIMARY KEY (id)
);

CREATE TABLE ranter
(
  id serial NOT NULL,
  display_name text NOT NULL,
  created_at timestamp without time zone DEFAULT now(),
  status_id integer,
  password_hashed text NOT NULL,
  CONSTRAINT ranter_pkey PRIMARY KEY (id),
  CONSTRAINT ranter_status_id_fkey FOREIGN KEY (status_id)
      REFERENCES status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ranter_display_name_uniq UNIQUE (display_name)
);

CREATE TABLE rant
(
  id serial NOT NULL,
  tagline text NOT NULL,
  body text NOT NULL,
  created_at timestamp without time zone DEFAULT now(),
  status_id integer,
  ranter_id integer,
  CONSTRAINT rant_pkey PRIMARY KEY (id),
  CONSTRAINT rant_ranter_id_fkey FOREIGN KEY (ranter_id)
      REFERENCES ranter (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT rant_status_id_fkey FOREIGN KEY (status_id)
      REFERENCES status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT rant_tagline_body_uniq UNIQUE (tagline, body)
);

CREATE TABLE comment
(
  id serial NOT NULL,
  body text NOT NULL,
  created_at timestamp without time zone NOT NULL DEFAULT now(),
  tagline text NOT NULL,
  status_id integer,
  rant_id integer,
  ranter_id integer,
  CONSTRAINT comment_pkey PRIMARY KEY (id),
  CONSTRAINT comment_rant_id_fkey FOREIGN KEY (rant_id)
      REFERENCES rant (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT comment_ranter_id_fkey FOREIGN KEY (ranter_id)
      REFERENCES ranter (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT comment_status_id_fkey FOREIGN KEY (status_id)
      REFERENCES status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT comment_tagline_body_uniq UNIQUE (tagline, body)
);

CREATE TABLE vote_type
(
  id serial NOT NULL,
  name text NOT NULL,
  description text NOT NULL,
  status_id integer,
  CONSTRAINT vote_type_pkey PRIMARY KEY (id),
  CONSTRAINT vote_type_status_id_fkey FOREIGN KEY (status_id)
      REFERENCES status (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE rant_vote
(
  rant_id integer NOT NULL,
  vote_type_id integer,
  ranter_id integer NOT NULL,
  CONSTRAINT rant_vote_pkey PRIMARY KEY (ranter_id, rant_id),
  CONSTRAINT rant_votes_rant_id_fkey FOREIGN KEY (rant_id)
      REFERENCES rant (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT rant_votes_ranter_id_fkey FOREIGN KEY (ranter_id)
      REFERENCES ranter (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT rant_votes_vote_type_id_fkey FOREIGN KEY (vote_type_id)
      REFERENCES vote_type (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE comment_vote
(
  comment_id integer NOT NULL,
  vote_type_id integer,
  ranter_id integer NOT NULL,
  CONSTRAINT comment_vote_pkey PRIMARY KEY (comment_id, ranter_id),
  CONSTRAINT comment_votes_comment_id_fkey FOREIGN KEY (comment_id)
      REFERENCES rant (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT comment_votes_ranter_id_fkey FOREIGN KEY (ranter_id)
      REFERENCES ranter (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT comment_votes_vote_type_id_fkey FOREIGN KEY (vote_type_id)
      REFERENCES vote_type (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE tag
(
  id serial NOT NULL,
  name text NOT NULL,
  CONSTRAINT tag_pkey PRIMARY KEY (id),
  CONSTRAINT name_key UNIQUE (name)
);

CREATE TABLE rant_tags
(
  rant_id integer NOT NULL,
  tag_id integer NOT NULL,
  CONSTRAINT rant_tag_pkey PRIMARY KEY (rant_id, tag_id),
  CONSTRAINT rant_tags_rant_id_fkey FOREIGN KEY (rant_id)
      REFERENCES rant (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT rant_tags_tag_id_fkey FOREIGN KEY (tag_id)
      REFERENCES tag (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE OR REPLACE VIEW rant_tags_view AS 
 SELECT r.id AS rant_id,
    r.tagline,
    r.body,
    r.created_at,
    r.status_id,
    r.ranter_id,
    t.name AS tag_name,
    t.id AS tag_id
   FROM rant r
   JOIN rant_tags rt ON r.id = rt.rant_id
   JOIN tag t ON t.id = rt.tag_id;

CREATE TABLE LOGS
   (DATE    TIMESTAMP NOT NULL,
    LOGGER  VARCHAR(50) NOT NULL,
    LEVEL   VARCHAR(10) NOT NULL,
    MESSAGE VARCHAR(1000) NOT NULL
   );

CREATE UNIQUE INDEX tag_lower_idx
  ON tag
  USING btree
  (lower(name) COLLATE pg_catalog."default");
