﻿ALTER TABLE status
  OWNER TO dbtester;
GRANT ALL ON TABLE status TO dbtester;
GRANT SELECT ON TABLE status TO dbtester;

ALTER TABLE ranter
  OWNER TO dbtester;
GRANT ALL ON TABLE ranter TO dbtester;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE ranter TO dbtester;

ALTER TABLE rant
  OWNER TO dbtester;
GRANT ALL ON TABLE rant TO dbtester;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE rant TO dbtester;

ALTER TABLE comment
  OWNER TO dbtester;
GRANT ALL ON TABLE comment TO dbtester;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE comment TO dbtester;

ALTER TABLE vote_type
  OWNER TO dbtester;
GRANT ALL ON TABLE vote_type TO dbtester;
GRANT SELECT ON TABLE vote_type TO dbtester;

ALTER TABLE rant_vote
  OWNER TO dbtester;
GRANT ALL ON TABLE rant_vote TO dbtester;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE rant_vote TO dbtester;

ALTER TABLE comment_vote
  OWNER TO dbtester;
GRANT ALL ON TABLE comment_vote TO dbtester;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE comment_vote TO dbtester;

ALTER TABLE tag
  OWNER TO dbtester;
GRANT ALL ON TABLE tag TO dbtester;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE tag TO dbtester;

ALTER TABLE rant_tags
  OWNER TO dbtester;
GRANT ALL ON TABLE rant_tags TO dbtester;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE rant_tags TO dbtester;

ALTER TABLE rant_tags_view
  OWNER TO dbtester;
GRANT ALL ON TABLE rant_tags_view TO dbtester;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE rant_tags_view TO dbtester;


ALTER TABLE comment_id_seq
  OWNER TO dbtester;
GRANT ALL ON TABLE comment_id_seq TO dbtester;
GRANT SELECT, UPDATE ON TABLE comment_id_seq TO dbtester;

ALTER TABLE rant_id_seq
  OWNER TO dbtester;
GRANT ALL ON TABLE rant_id_seq TO dbtester;
GRANT SELECT, UPDATE ON TABLE rant_id_seq TO dbtester;

ALTER TABLE ranter_id_seq
  OWNER TO dbtester;
GRANT ALL ON TABLE ranter_id_seq TO dbtester;
GRANT SELECT, UPDATE ON TABLE ranter_id_seq TO dbtester;

ALTER TABLE tag_id_seq
  OWNER TO dbtester;
GRANT ALL ON TABLE tag_id_seq TO dbtester;
GRANT SELECT, UPDATE ON TABLE tag_id_seq TO dbtester;
