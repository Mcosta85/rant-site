# Rantsite

This is the main repository for code of the Rantsite web app. We may consider open sourcing this some day, but for now this documentation will suffice.

## Commit Policy
When committing any changes to this repo, do so in your branch which is either named for the issue ticket you are fixing, or the feature you are adding.

Any commits need to include the unit tests added in the test directory in the project. If your code is untestable, take a moment to think about how you can change your code to be more testable. If you still deem it not testable, consult your PM or senior engineer for approval to push your changes. Commits without tests will be mocked and the committer brutally ridiculed.

Code should not be pushed directly to the CI branch. This ends up being what goes on the site, and we don't particularly like looking bad when a commit breaks the build. Thoroughly test on your local environment then push to a staging branch where QA will validate your changes.

## Testing Policy
Testing is important here. As mentioned above, no code will be accepted without tests. The goal in crafting tests will be to test all code paths you've added. It's not necessary to test with 10,000,000 kinds of invalid data, But make sure the validation/sanitizing of that data works under a case or two. The goal is 100% code coverage in our tests. As mentioned above, if you cannot test all code paths, speak with your PM or senior engineer and they'll review your assessment and either allow your commit or help you make fill out your testing. 

## Build Policy
We use continuous integration here. As such, our CI branch in this repo will be what gets built and pushed to the site. Don't push nonsense to the CI branch. We will find you, we have a very particular set of skills. Skills that make us a nightmare for coders like you.

# Sample Usage
curl -X POST -H "Content-Type: application/json" -d '{"display-name":"NAME", "password":"PASSWORD"}' URL/api/ranters