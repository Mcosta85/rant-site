(ns rantsite.models.rant-test
  (:use clojure.test)
  (:require [rantsite.utilities.fixtures :as fixture]
            [rantsite.utilities.common :as utils]
            [rantsite.models.rant :as model]))

(use-fixtures :each fixture/rant-each-fixture)

(deftest create-test
  (testing "valid input returns true"
    (is
       (= true
          (:success (model/create! {:ranter-id 1
                                    :tagline "blah"
                                    :body "blah"}
                                   utils/test-spec)))))

  (testing "invalid ranter id should return false"
    (is
      (= false
         (:success (model/create! {:ranter-id 0
                                   :tagline "test"
                                   :body "test"}
                                  utils/test-spec)))))

  (testing "text ranter id should return false"
    (is
      (= false
         (:success (model/create! {:ranter-id "1"
                                   :tagline "test"
                                   :body "test"}
                                  utils/test-spec)))))

  (testing "blank tagline should throw exception"
    (is
      (thrown? AssertionError
               (model/create! {:ranter-id 1
                               :tagline ""
                               :body "not empty"}
                              utils/test-spec))))

  (testing "blank body should throw exception"
    (is
      (thrown? AssertionError
               (model/create! {:ranter-id 1
                               :tagline "not empty"
                               :body ""}
                              utils/test-spec))))

  (testing "duplicate input should return false"
    (is
      (= false
         (:success (model/create! {:ranter-id 1
                                   :tagline "Rant Test"
                                   :body "Rant Test"}
                                  utils/test-spec))))))

(deftest get-one-test
  (testing "valid id returns 1 rant"
    (is
      (= 1
         (count (:rant (model/get-one 1
                                      utils/test-spec))))))
                    

  (testing "negative rant id should return success true"
    (is
      (= true
         (:success (model/get-one -5
                                  utils/test-spec)))))

  (testing "text rant id should throw AssertionError"
    (is
      (thrown? AssertionError
               (model/get-one "beef fungus"
                              utils/test-spec)))))

(deftest get-all-by-ranter-test
  (testing "valid ranter with rant should return 1 rant"
    (is
      (= 1
         (count (:rants (model/get-all-by-ranter 
                          1
                          utils/test-spec))))))

  (testing "negative ranter id should return success true"
    (is
      (= true
         (:success (model/get-all-by-ranter -10
                                            utils/test-spec)))))

  (testing "text ranter id should throw AssertionError"
    (is
      (thrown? AssertionError
               (model/get-all-by-ranter "ME"
                                        utils/test-spec)))))

(deftest hard-delete-test
  (testing "valid rant should return 1 row affected"
    (is
      (= 1
         (:rows-affected (model/hard-delete! 1
                                             utils/test-spec)))))

  (testing "negative rant id should return false"
    (is
      (= false
         (:success (model/hard-delete! -5
                                       utils/db-spec)))))

  (testing "text rant id should return false"
    (is
      (= false
         (:success (model/hard-delete! "-5"
                                       utils/test-spec))))))

(deftest soft-delete-test
  (testing "valid rant should return 1 row affected"
    (is
      (= 1
         (:rows-affected (model/soft-delete! 1
                                             utils/test-spec)))))

  (testing "negative rant id should return false"
    (is
      (= false
         (:success (model/soft-delete! -5
                                       utils/test-spec)))))

  (testing "text rant id should return false"
    (is
     (= false
        (:success (model/soft-delete! "-5"
                                      utils/test-spec))))))

(deftest update-test
  (testing "valid id and input should return true"
    (is
      (= true
         (:success (model/update! 1 
                                  {:body "new"}
                                  utils/test-spec)))))

  (testing "negative rant id should return false"
    (is
      (= false
         (:success (model/update! -5 
                                  {:body "none"}
                                  utils/test-spec)))))

  (testing "text rant id should return false"
    (is
      (= false
         (:success (model/update! "one" 
                                  {:body "none"}
                                  utils/test-spec)))))

  (testing "duplicate key should return false"
    (is
      (= false
         (:success (model/update! 2 
                                  {:id 1}
                                  utils/test-spec)))))

  (testing "invalid status id should return false"
    (is
      (= false
         (:success (model/update! 1 
                                  {:status-id 100}
                                  utils/test-spec)))))

  (testing "invalid column should return false"
    (is
      (= false
         (:success (model/update! 1
                                  {:no-column "blah"}
                                  utils/test-spec)))))

  (testing "setting id to nil should return false"
    (is
      (= false
         (:success (model/update! 1
                                  {:id nil}
                                  utils/test-spec)))))

  (testing "empty update map should throw AssertionError"
    (is
      (thrown? AssertionError
               (model/update! 1 
                              {}
                              utils/test-spec)))))
