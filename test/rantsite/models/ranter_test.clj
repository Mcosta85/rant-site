(ns rantsite.models.ranter-test
  (:use clojure.test)
  (:require [rantsite.models.ranter :as model]
            [rantsite.utilities.common :as utils]
            [rantsite.utilities.fixtures :as fixture]))

(use-fixtures :each fixture/ranter-each-fixture)

(deftest create-test
  (testing "valid input should return a new id"
    (is
      (= true
        (contains? (model/create! {:display-name "Testing"
                                   :password "TestPass"}
                                   utils/test-spec)
                                   :id))))

  (testing "empty display name show throw Assertion Error"
    (is
      (thrown? AssertionError
               (model/create! {:display-name ""
                               :password "meh"}
                               utils/test-spec))))

  (testing "empty password should throw Assertion Error"
    (is
      (thrown? AssertionError
               (model/create!  {:display-name "Create Test"
                                :password ""}
                               utils/test-spec))))

  (testing "duplicate input returns false"
    (is
      (= false
         (:success (model/create! {:display-name "Testing"
                                   :password "TestPass"}
                                  utils/test-spec))))))

(deftest get-one-test
  (testing "valid id should return ranter's name"
    (is
      (= "Test Ranter"
         (get-in (model/get-one 1
                                utils/test-spec)
                 [:ranter :display_name]))))

  (testing "negative ranter id should return success true"
    (is
      (= true
         (:success (model/get-one -5
                                  utils/test-spec)))))

  (testing "text ranter-id should throw AssertionError"
    (is
      (thrown? AssertionError
               (model/get-one "test"
                              utils/test-spec)))))

(deftest update-test
  (testing "valid data should return 1 row affected"
    (is
      (= 1
         (:rows-affected (model/update! 1 
                                        {:display-name "Update Test 1"}
                                        utils/test-spec)))))

  (testing "negative ranter id should return false"
    (is
      (= false
         (:success (model/update! -10 
                                  {:display-name "dfsedzC"}
                                  utils/test-spec)))))

  (testing "text ranter id should return false"
    (is
      (= false
         (:success (model/update! "test" 
                                  {:display-name "bob"}
                                  utils/test-spec)))))

  (testing "setting id to text should return false"
    (is
      (= false
         (:success (model/update! 1 
                                  {:id "Beef Fungus"}
                                  utils/test-spec)))))

  (testing "setting id to nil should return false"
    (is
      (= false
         (:success (model/update! 1 
                                  {:id nil}
                                  utils/test-spec)))))

  (testing "setting display name to nil should return false"
    (is
      (= false
         (:success (model/update! 1 
                                  {:display-name nil}
                                  utils/test-spec)))))

  (testing "adding a new column should return false"
    (is
       (= false
          (:success (model/update! 1 
                                   {:new-column "New"}
                                   utils/test-spec)))))

  (testing "empty update map should throw AssertionError"
    (is
      (thrown? AssertionError
               (model/update! 1
                              {}
                              utils/test-spec))))

  (testing "duplicate input returns false"
    (model/create! {:display-name "update test dupe"
                    :password "test"}
                   utils/test-spec)
    (is
      (= false
         (:success (model/update! 1
                                  {:display-name "update test dupe"}
                                  utils/test-spec))))))


(deftest soft-delete-test
  (testing "valid id should return true"
    (is
      (= true
         (:success (model/soft-delete! 1
                                       utils/test-spec)))))

  (testing "invalid id should return false"
    (is
      (= false
         (:success (model/soft-delete! 0
                                       utils/test-spec)))))

  (testing "test id should return false"
    (is
      (= false
         (:success (model/soft-delete! "test"
                                       utils/test-spec))))))

(deftest hard-delete-test
  (testing "valid id should return true"
    (is
      (= true
         (:success (model/hard-delete! 1
                                       utils/test-spec)))))

  (testing "invalid id should return false"
    (is
      (= false
         (:success (model/hard-delete! 0
                                       utils/test-spec)))))

  (testing "text id should return false"
    (is
      (= false
         (:success (model/hard-delete! "bob"
                                       utils/test-spec))))))
