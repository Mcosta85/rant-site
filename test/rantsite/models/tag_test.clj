(ns rantsite.models.tag-test
  (:use clojure.test)
  (:require [rantsite.utilities.common :as utils]
            [rantsite.models.tag :as model]
            [rantsite.utilities.fixtures :as fixture]))

(use-fixtures :each fixture/tag-each-fixture)

(deftest create-test
  (testing "Valid info returns 1 affected row"
    (is
      (= 1
        (:rows-affected
          (model/create! [["#Testing"]]
                         utils/test-spec)))))

  (testing "2 tags returns 2 affected rows"
    (is
      (= 2
         (count (model/create! [["#Test"] ["#MoreTest"]]
                               utils/test-spec)))))

  (testing "Empty input throws exception"
    (is
      (thrown? AssertionError
         (model/create! {}
                        utils/test-spec))))

  (testing "duplicate tag returns false"
    (is
      (= false
         (:success (model/create! [["#Testing"]]
                                  utils/test-spec)))))

  (testing "duplicate, upper case returns false"
    (is
      (= false
         (:success (model/create! '(["#TESTING"])
                                  utils/test-spec))))))

(deftest get-one-test
  (testing "Valid tag id returns true"
    (is
      (= true
         (:success (model/get-one 1
                                  utils/test-spec)))))

  (testing "Valid tag name returns tag id"
    (is
      (= true
         (:success (model/get-one "#Testing"
                                  utils/test-spec)))))

  (testing "Upper case tag name returns tag id"
      (is
        (= true
           (:success (model/get-one "#TESTING"
                                    utils/test-spec)))))

    (testing "invalid tag values returns success true"
      (is
        (= true
           (:success (model/get-one 0
                                    utils/test-spec)))))

    (testing "invalid tag value returns no other data"
      (is
        (= "Missing"
           (:name (model/get-one 0
                                 utils/test-spec) 
                  "Missing"))))

    (testing "nil tag value returns false"
      (is
        (= false
           (:success (model/get-one nil
                                    utils/test-spec)))))

    (testing "empty tag value returns no data"
      (is
        (= "Missing"
           (:name (model/get-one ""
                                 utils/test-spec) 
                  "Missing")))))

(deftest get-all-test
  (testing "create 2 rows, should return 2 rows"
     (model/create! ["#Test" "#MoreTest"]
                    utils/test-spec))

      (is
        (= 2
           (count (model/get-all utils/default-page-size
                                 utils/default-offset
                                 utils/test-spec))))

  (testing "create 21 rows, default page-size should return 20 rows"
      (model/create! [["#Test"] ["#Test2"] ["#Test3"] ["#Test4"]
                       ["#Test5"] ["#Test6"] ["#Test7"] ["#Test8"]
                       ["#Test9"] ["#Test10"] ["#Test11"] ["#Test12"]
                       ["#Test13"] ["#Test14"] ["#Test15"] ["#Test16"]
                       ["#Test17"] ["#Test18"] ["#Test19"] ["#Test20"]
                       ["#NotShow"]]
                     utils/test-spec)
      (is
        (= 20
           (count (:tags (model/get-all utils/default-page-size
                                        utils/default-offset
                                        utils/test-spec))))))

  (testing "create 21 rows, default offset 1 should return 1 row"
    (model/create! [["#Test"] ["#Test2"] ["#Test3"] ["#Test4"]
                    ["#Test5"] ["#Test6"] ["#Test7"] ["#Test8"]
                    ["#Test9"] ["#Test10"] ["#Test11"] ["#Test12"]
                    ["#Test13"] ["#Test14"] ["#Test15"] ["#Test16"]
                    ["#Test17"] ["#Test18"] ["#Test19"] ["#Test20"]
                    ["#NotShow"]]
                   utils/test-spec)

    (is
      (= 1
         (count (:tags (model/get-all utils/default-page-size
                                      1
                                      utils/test-spec))))))

  (testing "offset 2 should return 0 rows"
    (is
      (= 0
         (count (:tags (model/get-all utils/default-page-size 
                                      2
                                      utils/test-spec))))))

  (testing "page-size 1 returns 1 row"
    (is
      (= 1
         (count (:tags (model/get-all 1 
                                      utils/default-offset
                                      utils/test-spec))))))

  (testing "negative page-size should throw AssertionError"
    (is
      (thrown? AssertionError
         (model/get-all -5 
                        utils/default-offset
                        utils/test-spec)))))
