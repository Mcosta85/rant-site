(ns rantsite.models.comment-test
  (:use clojure.test)
  (:require [rantsite.utilities.fixtures :as fixture]
            [rantsite.utilities.common :as utils]
            [rantsite.models.comment :as model]))

(use-fixtures :each fixture/comment-each-fixture)

(deftest create-test
  (testing "valid input should return success true"
    (is
      (= true
         (:success (model/create! 1 
                                  {:body "Create Comment Test"
                                   :tagline "CCT"
                                   :ranter-id 1}
                                  utils/test-spec)))))

  (testing "duplicate input should return false"
    (is
      (= false
         (:success (model/create! 1 
                                  {:body "Test comment"
                                   :tagline "Test tagline"
                                   :ranter-id 1}
                                  utils/test-spec)))))

  (testing "invalid ranter id should return false"
    (is
      (= false
         (:success (model/create! 1
                                  {:body "create test body"
                                   :tagline "testing create2"
                                   :ranter-id 0}
                                  utils/test-spec)))))

  (testing "text ranter id should return false"
    (is
      (= false
         (:success (model/create! 1
                                  {:body "create test body"
                                   :tagline "testing create3"
                                   :ranter-id "ranter"}
                                  utils/test-spec)))))

    (testing "blank ranter id should return false"
      (is
        (= false
           (:success (model/create! 1
                                    {:body "create test body"
                                     :tagline "testing create4"
                                     :ranter-id ""}
                                    utils/test-spec)))))

    (testing "blank body should throw assertion error"
      (is
        (thrown? AssertionError
          (model/create! 1
                         {:body ""
                          :tagline "testing create5"
                          :ranter-id 1}
                         utils/test-spec))))

    (testing "blank tagline should throw assertion error"
      (is
        (thrown? AssertionError
          (model/create! 1
                         {:body "test"
                          :tagline ""
                          :ranter-id 1}
                         utils/test-spec)))))

(deftest get-all-test
  (testing "valid input returns > 0 comments"
    (is
      (< 0
        (count (:comments (model/get-all 1
                                         utils/default-page-size
                                         utils/default-offset
                                         utils/test-spec))))))

  (testing "invalid id returns 0 comments"
    (is
      (= 0
         (count (:comments (model/get-all 0
                                          utils/default-page-size
                                          utils/default-offset
                                          utils/test-spec))))))

    (testing "test id should throw exception"
      (is
        (thrown? AssertionError
                 (model/get-all "GA"
                                utils/default-page-size
                                utils/default-offset
                                utils/test-spec)))))

(deftest update-test
  (testing "valid input returns true"
    (is
      (= 1
         (:rows-affected (model/update! 1
                                        {:tagline "update"}
                                        utils/test-spec)))))

  (testing "Non existing comment id returns false"
    (is
      (= false
         (:success (model/update! 0
                                  {:body "meh"}
                                  utils/test-spec)))))

  (testing "text comment id returns false"
    (is
      (= false
         (:success (model/update! "bob"
                                  {:body "meh"}
                                  utils/test-spec)))))

  (testing "empty update map should thrown AssertionError"
    (is
      (thrown? AssertionError
         (model/update! 1
                        {}
                        utils/test-spec)))))

  (testing "extra keys in update map returns false"
    (is
      (= false
         (:success (model/update! 1
                                  {:extra "nope"}
                                  utils/test-spec)))))

  (testing "duplicate comment should return false"
    (model/create! 1 {:ranter-id 1
                      :tagline "test"
                      :body "test"}
                   utils/test-spec)

    (is
      (= false
         (:success (model/update! 1
                                  {:tagline "test"
                                   :body "test"}
                                  utils/test-spec)))))

(deftest soft-delete-test
  (testing "valid comment id returns 1 affected row"
    (is
      (= 1
         (:rows-affected (model/soft-delete! 1
                                             utils/test-spec)))))

  (testing "re-deleting valid id should return 1 affected row"
    (is
      (= 1
         (:rows-affected (model/soft-delete! 1
                                             utils/test-spec)))))

    (testing "invalid comment id returns false"
      (is
        (= false
           (:success (model/soft-delete! 0
                                         utils/test-spec)))))

    (testing "text comment id should return false"
      (is
        (= false
           (:success (model/soft-delete! "test"
                                         utils/test-spec)))))

    (testing "nil comment id should return false"
      (is
        (= false
           (:success (model/soft-delete! nil
                                         utils/test-spec))))))

(deftest hard-delete-test
  (testing "valid id returns 1 affected row"
    ;; No idea why, but this comment is created with id 2.
    ;; I checked every other test and they're all id 1. Probably
    ;; should investigate at some point, but this made the tests pass.
    (is
      (= 1
         (:rows-affected (model/hard-delete! 2
                                             utils/test-spec)))))

    (testing "invalid id returns false"
      (is
        (= false
           (:success (model/hard-delete! 0
                                         utils/test-spec)))))

    (testing "text comment id returns false"
      (is
        (= false
           (:success (model/hard-delete! "test-comment"
                                         utils/test-spec))))))
