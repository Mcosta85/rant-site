(ns rantsite.models.vote-test
  (:use clojure.test)
  (:require [rantsite.utilities.fixtures :as fixture]
            [rantsite.utilities.common :as utils]
            [rantsite.models.vote :as model]))

(use-fixtures :each fixture/vote-each-fixture)

(deftest create-test
  (testing "valid rant downvote returns success true"
    (fixture/delete-votes)
    (is
      (= true
         (:success (model/create! model/rant-object
                                  1
                                  1
                                  model/downvote
                                  utils/test-spec)))))

  (testing "valid rant upvote returns success true"
    (fixture/delete-votes)
    (is
      (= true
         (:success (model/create! model/rant-object
                                  1
                                  1
                                  model/upvote
                                  utils/test-spec)))))

  (testing "valid comment downvote returns success true"
    (fixture/delete-votes)
    (is
      (= true
         (:success (model/create! model/comment-object
                                  1
                                  1
                                  model/downvote
                                  utils/test-spec)))))

  (testing "valid comment upvote returns success true"
    (fixture/delete-votes)
    (is
      (= true
         (:success (model/create! model/comment-object
                                  1
                                  1
                                  model/upvote
                                  utils/test-spec)))))

  (testing "duplicate input should return false"
    (model/create! model/rant-object
                    1
                    1
                    model/downvote
                    utils/test-spec)
    (is
     (= false
        (:success (model/create! model/rant-object
                                 1
                                 1
                                 model/downvote
                                 utils/test-spec)))))

  (testing "invalid object returns false"
    (is
      (= false
         (:success (model/create! "test"
                                   1
                                   1
                                   model/upvote
                                   utils/test-spec)))))

  (testing "empty object throws assertion error"
    (is
      (thrown? AssertionError
               (model/create! ""
                             1
                             1
                             model/upvote
                             utils/test-spec))))

  (testing "nil object throws assertion error"
    (is
      (thrown? AssertionError
               (model/create! nil
                             1
                             1
                             model/upvote
                             utils/test-spec))))

  (testing "invalid ranter id returns false"
    (is
      (= false
         (:success (model/create! model/rant-object
                                  1000
                                  1
                                  model/upvote
                                  utils/test-spec)))))

  (testing "negative ranter id throws assertion error"
    (is
      (thrown? AssertionError
         (model/create! model/rant-object
                        -1
                        1
                        model/upvote
                        utils/test-spec))))

  (testing "nil ranter id throws assertion error"
    (is
      (thrown? AssertionError
         (model/create! model/rant-object
                        nil
                        1
                        model/upvote
                        utils/test-spec))))

  (testing "invalid rant id returns false"
    (is
      (= false
         (:success (model/create! model/rant-object
                                 1
                                 1000000
                                   model/upvote
                                   utils/test-spec)))))

  (testing "negative rant id throws assertion error"
    (is
      (thrown? AssertionError
         (model/create! model/rant-object
                        1
                        -1
                        model/upvote
                        utils/test-spec))))

  (testing "nil rant id throws assertion error"
    (is
      (thrown? AssertionError
         (model/create! model/rant-object
                        1
                        nil
                        model/upvote
                        utils/test-spec))))

  (testing "invalid vote type id returns false"
    (is
      (= false
         (:success (model/create! model/rant-object
                                  1
                                  1
                                  100000
                                  utils/test-spec)))))

  (testing "negative vote type id throws assertion error"
    (is
      (thrown? AssertionError
         (model/create! model/rant-object
                        1
                        1
                       -1
                       utils/test-spec))))

  (testing "nil vote type id throws assertion error"
    (is
      (thrown? AssertionError
         (model/create! model/rant-object
                        1
                        1
                        nil
                        utils/test-spec)))))

(deftest get-count-test
  (testing "valid info returns success true"
    (model/create! model/rant-object
                    1
                    1
                    model/upvote
                    utils/test-spec))

    (is
      (= 1
         (:Upvote (model/get-count model/rant-object
                                   1
                                   utils/test-spec))))
  
  (testing "invalid object returns false"
    (is
      (= false
         (:success (model/get-count "wrong" 
                                    1
                                    utils/test-spec)))))

  (testing "empty object throws Assertion Error"
    (is
      (thrown? AssertionError
               (model/get-count "" 
                                1
                                utils/test-spec))))

  (testing "nil object throws Assertion Error"
    (is
      (thrown? AssertionError
               (model/get-count nil 
                                1
                                utils/test-spec))))

  (testing "impossible id returns Assertion Error"
    (is
      (thrown? AssertionError
         (model/get-count model/rant-object 
                          -1
                          utils/test-spec))))

  (testing "nil id returns Assertion Error"
    (is
      (thrown? AssertionError
         (model/get-count model/rant-object
                          nil
                          utils/test-spec))))

  (testing "non existant id returns true and no results"
    (is
      (= true
         (:success (model/get-count model/rant-object
                                    10000
                                    utils/test-spec))))))

(deftest update-test
  (testing "valid input returns true"
    (model/create! model/comment-object
                     1
                     1
                     model/downvote
                     utils/test-spec))

    (is
      (= true
         (:success (model/update! {:object model/comment-object
                                   :ranter-id  1
                                   :object-id  1
                                   :vote-type-id  model/upvote}
                                  utils/test-spec))))

  (testing "invalid object returns false"
    (is
      (= false
         (:success (model/update! {:object "test"
                                  :ranter-id 1
                                  :object-id 1
                                  :vote-type-id model/downvote}
                                  utils/test-spec)))))

  (testing "empty object throws assertion error"
    (is
      (thrown? AssertionError
               (model/update! {:object ""
                               :ranter-id 1
                               :rant-id 1
                               :vote-type-id model/upvote}
                              utils/test-spec))))

    (testing "nil object throws assertion error"
      (is
        (thrown? AssertionError
                 (model/update! {:object nil
                                 :ranter-id 1
                                 :rant-id 1
                                 :vote-type-id model/upvote}
                                utils/test-spec)))))
