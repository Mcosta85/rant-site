(ns rantsite.utilities.common-test
  (:use clojure.test)
  (:require [rantsite.utilities.common :as utils]))

(deftest not-nil-test
  (testing "nil input should return false"
    (is
        (= false
           (utils/not-nil? nil))))

  (testing "non nil input should return false"
    (is
      (= true
         (utils/not-nil? (not nil))))))

(deftest rows-affected-test
  (testing "zero should return false"
    (is
      (= false
         (utils/rows-affected? 0))))

  (testing "1 should return true"
    (is
      (= true
         (utils/rows-affected? 1)))))

(deftest parse-row-count-test
  (testing "'(1) returns success true"
    (is
      (= true
         (:success (utils/parse-row-count '(1))))))

  (testing "'(1) returns rows-affected 1"
    (is
      (= 1
         (:rows-affected (utils/parse-row-count '(1))))))

  (testing "'(0) returns success false"
    (is
      (= false
         (:success (utils/parse-row-count '(0))))))

  (testing "'(0) returns rows-affected 0"
    (is
      (= 0
         (:rows-affected (utils/parse-row-count '(0))))))

  (testing "non numeric input throws AssertionError"
    (is
      (thrown? AssertionError
               (utils/parse-row-count "test"))))

  (testing "nil input throws AssertionError"
    (is
      (= false
         (:success (utils/parse-row-count nil))))))
